import com.typesafe.scalalogging.LazyLogging
import io.circe.parser.decode
import model.MessageFormats._
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._

class StreamingJob(
    streamingContext: StreamingContext,
    config: Map[String, Object],
    topic: Array[String]) extends LazyLogging {

  def run(): Unit = {
    KafkaUtils
      .createDirectStream(
        streamingContext,
        PreferConsistent,
        Subscribe[String, String](topic, config))
      .map(r => (r.key(), r.value()))
      .mapValues(decode[Customer])
      .mapValues {
        case Left(e) => logger.error(e.getMessage)
        case Right(c) => logger.info(c.toString)
      }
  }
}
