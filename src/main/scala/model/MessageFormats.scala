package model
import io.circe.generic.semiauto._

object MessageFormats {
  case class Customer(name: String, id: Long)
  implicit val customerDecoder = deriveDecoder[Customer]
}
