#!/bin/bash

set -e

SKIP_TESTS=$1
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

kubectl config use-context minikube

VER=$(date +%Y%m%d%H%M%S)
eval $(minikube docker-env)

for yaml_file in $(find $SCRIPT_DIR/../minikube/*.yaml) ; do
    echo "Deploying $yaml_file..."
    kubectl apply -f ${yaml_file}
done

set +e
