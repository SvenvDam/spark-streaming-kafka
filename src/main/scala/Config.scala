import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.Seconds

object Config {
  val kafkaConfig = Map(
    "bootstrap.servers" -> "local-kafka-service:9092",
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "use_a_separate_group_id_for_each_stream",
    "auto.offset.reset" -> "latest",
    "enable.auto.commit" -> (false: java.lang.Boolean)
  )

  val kafkaTopic = Array("my-topic")

  val sparkName = "streaming-test"
  val streamingInterval = Seconds(1)
}
