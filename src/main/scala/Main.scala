import Config._
import org.apache.spark.SparkConf
import org.apache.spark.streaming.StreamingContext

object Main {

  val sparkConf = new SparkConf().setAppName(sparkName)
  val streamingContext = new StreamingContext(sparkConf, streamingInterval)
  val job = new StreamingJob(streamingContext, kafkaConfig, kafkaTopic)

  def main(args: Array[String]): Unit = {
    job.run()
  }
}
